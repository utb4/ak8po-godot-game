extends Sprite2D

# Wheel properties.
@onready var vehicle: RigidBody2D = null
var is_steering: bool = false
var max_angle: float = 0.0
var steering_speed: float = 0.0
var center_steering: bool = true
var power: float = 0.0
var grip: float = 0.0

# Dynamics tracking.
@onready var forward = -global_transform.y.normalized()
@onready var right = global_transform.x.normalized()
@onready var last_position = global_position
var linear_velocity: Vector2

func _physics_process(delta: float):
	# Update velocity and direction vectors.
	linear_velocity = global_position - last_position
	last_position = global_position
	forward = -global_transform.y.normalized()
	right = global_transform.x.normalized()

func drive(drive_input: float):
	# Apply force based on driving input, if power is set.
	if power != 0 and drive_input != 0:
		var drive_force = forward * power * vehicle.mass * drive_input
		vehicle.apply_impulse(drive_force, global_position - vehicle.global_position)

func steer(steering_input: float):
	# Calculate the desired steering angle.
	if is_steering:
		var target_angle = steering_input * max_angle
		if steering_input == 0 and center_steering:
			target_angle = 0
		rotation_degrees = lerp(rotation_degrees, target_angle, steering_speed)

func apply_lateral_forces():
	# Check if 'vehicle' is initialized
	if vehicle and vehicle.is_inside_tree():
		# Lateral forces for realistic tire behavior.
		var lateral_vel = linear_velocity.dot(right)
		vehicle.apply_impulse(-grip * lateral_vel * right, global_position - vehicle.global_position)
	else:
		print("Vehicle not initialized or not in scene tree.")

func set_properties(forklift: Node):
	# Set properties received from the parent vehicle.
	vehicle = forklift as RigidBody2D
	if vehicle:
		is_steering = get_name().begins_with("R")
		max_angle = 55.0 if is_steering else 0
		power = 1.0 if is_steering else 1.5  # Front wheels have less power.
		steering_speed = forklift.steering_speed
		center_steering = forklift.center_steering
		grip = forklift.grip
	else:
		print("Invalid vehicle reference passed to wheel.")
