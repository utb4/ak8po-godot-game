extends RigidBody2D

# Exported variables for easy adjustment in the editor.
@export var grip: float = 3.0
@export var steering_speed: float = 0.5
@export var steering_speed_decay: float = 0.1
@export var center_steering: bool = true
@export var air_resistance: float = 10

# Internal variables.
var right: Vector2
var wheel_group: String
var picked_stone: Node = null
var stone_offset: Vector2 = Vector2.ZERO
var stone_rotation_offset: float = 0.0
var original_parent = null
var screen_size: Vector2
var stop = false

func _ready():
	# Initial setup of wheel group and direction vectors.
	right = global_transform.x.normalized()
	wheel_group = "wheels-%d" % get_instance_id()
	
	# Add each wheel to the group and configure properties.
	var wheels = get_node("Wheels").get_children()
	for wheel in wheels:
		wheel.add_to_group(wheel_group)
		wheel.call("set_properties", self)
	
	set_contact_monitor(true)
	set_max_contacts_reported(10)
	connect("body_entered", Callable(self, "_on_body_entered"))
	#$ForkZone2DL.connect("area_entered", Callable(self, "_on_area_entered"))
	
	# Get the screen size
	screen_size = get_viewport().get_visible_rect().size

func _physics_process(delta: float):
	if stop:
		linear_velocity = Vector2.ZERO  # Stop all movement
		angular_velocity = 0.0  # Stop rotation
		
		return  # Exit the function if the game is stopped
		
	# Handle inputs for driving and steering.
	var drive_input = Input.get_action_strength("accelerate") - Input.get_action_strength("decelerate")
	var steering_input = Input.get_action_strength("steer_right") - Input.get_action_strength("steer_left")
	
	# Adjust steering based on speed.
	steering_input /= (0.01 * steering_speed_decay * linear_velocity.length() + 1)
	# Update the wheel group with the new inputs.
	get_tree().call_group(wheel_group, "drive", drive_input)
	get_tree().call_group(wheel_group, "steer", steering_input)
	get_tree().call_group(wheel_group, "apply_lateral_forces")
	
	# Apply simplified air resistance.
	var vel = linear_velocity * 0.005
	apply_central_impulse(-air_resistance * vel)
	
	if Input.is_action_just_pressed("pickup_release"):
		if picked_stone:
			release_stone()
		else:
			check_both_fork_zones("StoneBody2D")
			
	# Move the picked stone with the forklift
	if picked_stone:
		var new_transform = Transform2D(rotation, global_position + stone_offset.rotated(rotation))
		picked_stone.global_transform = new_transform
	
	# Clamp the position to keep the RigidBody2D within the screen boundaries
	var clamped_position = global_position
	clamped_position.x = clamp(clamped_position.x, 0, screen_size.x)
	clamped_position.y = clamp(clamped_position.y, 0, screen_size.y)
	global_position = clamped_position

func _on_body_entered(body):
	print("Collision")

# Checks that object is placed on both forks for possible pickup
func check_both_fork_zones(body_name):
	print("CheckForPickup")
	var area2d_left = $ForkZoneLeft
	var area2D_right = $ForkZoneRight
	var overlapping_bodies_left = area2d_left.get_overlapping_bodies()
	var overlapping_bodies_right = area2D_right.get_overlapping_bodies() 
	for body_left in overlapping_bodies_left:
		if body_left.name == body_name:
			for body_right in overlapping_bodies_right:
				if body_right.name == body_name:    
					picked_stone = body_right
					stone_offset = (picked_stone.global_position - global_position).rotated(rotation)
					#stone_rotation_offset = picked_stone.rotation
					original_parent = picked_stone.get_parent()
					original_parent.remove_child(picked_stone)  # Remove from original parent
					add_child(picked_stone)  # Add to the forklift
					print("Stone picked up")                    
					break

func pickup_stone():
	if picked_stone:
		picked_stone.gravity_scale = 0
		picked_stone.linear_velocity = Vector2.ZERO

func release_stone():
	if picked_stone:
		picked_stone.gravity_scale = 1
		remove_child(picked_stone)  # Remove from the forklift
		original_parent.add_child(picked_stone)  # Add back to the original parent
		var new_transform = Transform2D(rotation, global_position + stone_offset.rotated(rotation))
		picked_stone.global_transform = new_transform
		picked_stone.rotation = rotation + stone_rotation_offset
		# Call the main scene to handle the stone release
		var main_scene = get_parent()
		if main_scene and main_scene.has_method("on_stone_released"):
			main_scene.on_stone_released(picked_stone)
		picked_stone = null
		stone_offset = Vector2.ZERO
		stone_rotation_offset = 0.0
		original_parent = null

func _on_fork_zone_2d_area_entered(area):
	print("Yes in area", area.name)
	if area.name == "Stone" and not picked_stone:
		print("Stone entered the pickup area")
		picked_stone = area
		stone_offset = area.global_position - global_position
