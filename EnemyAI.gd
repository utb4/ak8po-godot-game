extends Sprite2D

# Variables for movement
var speed = 50
var direction = Vector2.ZERO
var screen_size
var stop = false


func _ready():
	# Get the size of the viewport
	screen_size = get_viewport_rect().size
	# Set an initial random direction
	direction = Vector2(randf_range(-1, 1), randf_range(-1, 1)).normalized()
	

func _process(delta):
	if stop: 
		return
	# Move the sprite in the current direction
	position += direction * speed * delta
	
	# Check if the sprite is out of bounds and adjust the direction if necessary
	if position.x < 0 or position.x > screen_size.x:
		direction.x = -direction.x
	if position.y < 0 or position.y > screen_size.y:
		direction.y = -direction.y
	
	# Clamp the position to keep the sprite within the screen boundaries
	position.x = clamp(position.x, 0, screen_size.x)
	position.y = clamp(position.y, 0, screen_size.y)
	
	
	# Change direction randomly over time
	if randf() < 0.01:
		direction = Vector2(randf_range(-1, 1), randf_range(-1, 1)).normalized()

func randf_range(min, max):
	return min + randf() * (max - min)
