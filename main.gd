extends Node2D  # This script should be attached to the 'main' Node2D node


# Preload the Stone scene
var stone_scene: PackedScene = preload("res://stone.tscn")
var tree_scene: PackedScene = preload("res://tree_small.tscn")
var enemy_scene: PackedScene = preload("res://enemy.tscn")

# Define the detection area properties
var detection_area_position: Vector2 = Vector2(0, 0)
var detection_area_size: Vector2 = Vector2(200, 200)
var safe_margin: float = 200.0  # Additional margin to avoid spawning too close

var screen_size
var score: int = 0
var spawned_positions: Array = []
var game_over = false;

func _ready():
	$GameOver.hide()
	randomize()  # This initializes the random number generator with a different seed
	spawn_trees()
	spawn_stone()
	add_collection_area()
	update_score_label()
	screen_size = get_viewport_rect().size
	
func spawn_trees():
	for i in range(5):
		spawn_one_tree()

func spawn_one_tree():
		var tree_instance = tree_scene.instantiate()
		var random_position: Vector2 = get_random_position()
		tree_instance.position = random_position
		spawned_positions.append(random_position)
		add_child(tree_instance)
		
func spawn_stone():
	var stone_instance = stone_scene.instantiate()  # 'instantiate' replaces 'instance'
	if stone_instance == null:
		print("Failed to instantiate Stone. Check the scene path and type.")
		return

	var position: Vector2 = get_random_position()
	stone_instance.position = position
	spawned_positions.append(position)

	# Add the new instance to the main node
	add_child(stone_instance)

func get_random_position() -> Vector2:
	var position: Vector2
	var is_valid_position: bool = false
	var attempts = 0
	var max_attempts = 100  # Prevent infinite loop

	while not is_valid_position and attempts < max_attempts:
		position = Vector2(randf() * get_viewport_rect().size.x, randf() * get_viewport_rect().size.y)
		attempts += 1

		# Check if the position is outside the detection area, top area, and has sufficient margin
		if not is_inside_detection_area(position) and not is_in_top_area(position) and has_sufficient_margin(position):
			is_valid_position = true

	return position

func is_inside_detection_area(position: Vector2) -> bool:
	var left = detection_area_position.x - detection_area_size.x - safe_margin
	var right = detection_area_position.x + detection_area_size.x + safe_margin
	var top = detection_area_position.y - detection_area_size.y - safe_margin
	var bottom = detection_area_position.y + detection_area_size.y + safe_margin

	return position.x > left and position.x < right and position.y > top and position.y < bottom

func is_in_top_area(position: Vector2) -> bool:
	# Define the top margin area height
	var top_area_height = 100
	return position.y < top_area_height

func has_sufficient_margin(position: Vector2) -> bool:
	var margin = 200
	for spawned_position in spawned_positions:
		if position.distance_to(spawned_position) < margin:
			return false
	return true

func add_collection_area():
	# Create a new Area2D node
	var area = Area2D.new()
	area.name = "DetectionArea"
	area.z_index = -5
	
	# Create a collision shape for the area
	var collision_shape = CollisionShape2D.new()
	var shape = RectangleShape2D.new()
	shape.extents = detection_area_size / 2  # Set the size of the rectangle
	collision_shape.shape = shape

	# Add the collision shape to the area
	area.add_child(collision_shape)

	# Create a ColorRect to visualize the area (optional, for debugging)
	var color_rect = ColorRect.new()
	color_rect.color = Color(0, 1, 0, 0.2)  # Green color with 20% transparency
	color_rect.size = detection_area_size  # Set the size of the rectangle
	area.add_child(color_rect)

	# Create a Line2D to add a border around the ColorRect
	var border = Line2D.new()
	border.width = 2  # Border width
	border.default_color = Color(1, 1, 1)  # White border color
	border.add_point(Vector2(-detection_area_size.x, -detection_area_size.y))  # Top-left
	border.add_point(Vector2(detection_area_size.x, -detection_area_size.y))   # Top-right
	border.add_point(Vector2(detection_area_size.x, detection_area_size.y))    # Bottom-right
	border.add_point(Vector2(-detection_area_size.x, detection_area_size.y))   # Bottom-left
	border.add_point(Vector2(-detection_area_size.x, -detection_area_size.y))  # Back to top-left to close the border

	# Add the border to the Area2D node
	area.add_child(border)

	# Set the position of the area (you can adjust this as needed)
	area.position = detection_area_position

	# Add the area to the main node
	add_child(area)

	# Connect the area entered signal to a function
	area.body_entered.connect(_on_area_body_entered)

# Function to handle the body entered signal
func _on_area_body_entered(body):
	if body.name == "Forklift":
		print("Forklift entered the area")

func update_score_label():
	$ScoreLabel.text = "Score: " + str(score)

func check_stone_in_area(stone):
	var left = detection_area_position.x - detection_area_size.x
	var right = detection_area_position.x + detection_area_size.x
	var top = detection_area_position.y - detection_area_size.y
	var bottom = detection_area_position.y + detection_area_size.y

	var pos = stone.global_position

	if pos.x > left and pos.x < right and pos.y > top and pos.y < bottom:
		return true
	return false

func on_stone_released(stone):
	print("Stone released")
	if check_stone_in_area(stone):
		print("Stone released in area")
		$CollectedSound.play()
		score += 1
		update_score_label()
		stone.queue_free()
		spawn_stone()
		spawn_one_tree()
			
		
	
func spawn_enemy():
	var enemy_instance = enemy_scene.instantiate()	
	add_child(enemy_instance)
	
func _process(delta):
	var forklift = get_node("Forklift").position
	var enemy = get_node("EnemyAI").global_position
	#print(forklift)
	#print(enemy)
	
	var collision_threshold = 50
	 # Calculate the distance between forklift and enemy
	var distance = forklift.distance_to(enemy)
	# Check if the distance is within the threshold
	if distance <= collision_threshold:
		print("Collision detected!")
		game_over = true
		$GameOver.show()
		$EnemyAI.stop = true
		$Forklift.stop = true
		# Perform any collision response here

func _input(event):
	if game_over:
		return  # Do nothing if the game is over

